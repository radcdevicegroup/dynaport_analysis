function[Nstep,step_time,CV, Sind]=tandem_step_detection(AP_filt,Time,ML,AP,SampleRate)
%% input
%   1)AP_filt- filterd AP signal
%   2)Time- time vector
%   3)ML signal
%   4)AP signal
%   5)SampleRate
%   6)p- if p=1 the function is plotting a figure with the area under the PSD.
%% output
%   1)Nstep- the number of steps
%   2)step_time- mean step duration 
%   3)CV- Coefficient of variation for the step duration


pks=[];locs=[];
[pks,locs] =findpeaks((-AP_filt),'MinPeakDistance',SampleRate/2, 'MinPeakProminence', 0.05, 'MinPeakHeight', 0); %finding peaks in the filterd AP signal

%If no steps detected, try less strict peak finding
if isempty(locs)
    [pks,locs] =findpeaks((-AP_filt),'MinPeakDistance',SampleRate/2, 'MinPeakHeight', 0);
end

%if no steps still, return NaN for variables
if isempty(pks)
    Nstep = NaN;
    step_time = NaN;
    CV = NaN;
    Sind = [];
else
    %identify if first step is incorrect
    diffLocs = diff(locs);
    diffLim = mean(diffLocs) + 2*std(diffLocs);
    if diffLocs(1) > diffLim
        locs(1) = [];
        pks(1) = [];
    end


    Nstep=length(nonzeros(locs));
    Sind=nonzeros(locs);AllrmsML=[];
    for i=1:length(Sind)-1 % The rms for each step from the ML axis
    AllrmsML(i)=rms(ML(Sind(i):Sind(i+1)-1));
    end

    stdRMSML=std(AllrmsML); % the standard deviation between steps
    time_vec=diff(Sind)*Time(2,1);
    step_time=mean(time_vec); % mean step duration
    CV=(std(time_vec)/mean(time_vec))*100; %Coefficient of variation for the step duration
end
% 
% %-----figure
% if(p==1)
% figure;
% plot(Time,-AP,'LineWidth',3,'color','b');hold on;
% plot(Time,-AP_filt,'LineWidth',3,'color','k'); 
% plot(Time(nonzeros(locs)),nonzeros(pks),'o','MarkerSize',10,'MarkerFaceColor','r');
% set(gca, 'FontSize', 20); 
% ylim([-0.2 0.2]);
% xlabel('Time [s]');ylabel('Acceleration from AP axis [m/s^2]');
% end

end