function [stepInd, DATA, head] = readTandemAnalysis(p,fn)
%This Reads the Mistaken Files where Step Time CV is lost
%   Detailed explanation goes here

InputFile = [p fn];
fid = fopen(InputFile); outp = textscan(fid, '%s', 'Delimiter', '\n', 'CollectOutput', true); fclose(fid);

outp = outp{1};
head = outp(1:4);
nVar = 17;
DATA = zeros(1,nVar);
stepInd = [];

strFail = 'Failed Analysis';
failFlg = find(strncmp(outp,strFail,length(strFail)));



if isempty(failFlg)
    for II = 1:nVar
        nLine = II + 5;
        strInd = strfind(outp{nLine},':');
        DATA(II) = str2double(outp{nLine}(strInd+1:end));
    end
    
    
    txtStep = find(strncmp(outp,'Step Indices',12));
    if length(txtStep) > 1          %For Incorrect method where steps listed on multiple lines
        stepInd = zeros(1, length(txtStep));
        for JJ = 1:length(txtStep)
            tempInd = strfind(outp{txtStep(JJ)},':');
            stepInd(JJ) = str2double(outp{txtStep(JJ)}(tempInd+1:end));
        end
    else
        txtStart = strfind(outp{txtStep},':');
        txtDelim = strfind(outp{txtStep},',');
        nStep = length(txtDelim)+1;
        stepInd = zeros(1,nStep);
        stepInd(1) = str2double(outp{txtStep}(txtStart+1:txtDelim(1)-1));
        for JJ = 2:length(txtDelim)
            stepInd(JJ) = str2double(outp{txtStep}(txtDelim(JJ-1)+1:txtDelim(JJ)-1));
        end
        stepInd(nStep) = str2double(outp{txtStep}(txtDelim(nStep-1)+1:end));

    end
end

