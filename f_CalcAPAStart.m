function APA_Start=f_CalcAPAStart(Signal,Time,Samplerate,Plot)
%APA_Start - time of APA start in millisec (Time = 0 at the cue signal)
if nargin==3
    Plot=0;
end
if isrow(Signal)
    Signal=Signal';
end
CueIndex=find(Time==0);
ipt=findchangepts(Signal,'Statistic','linear','MinThreshold',range(Signal)/10);
% figure;findchangepts(Signal,'Statistic','linear','MinThreshold',range(Signal)/10);
%%% In case that the first linear part prior to the APA start is devided
%%% to 2 parts (not compleatly quiet standing), removing the initial linear
%%% part if there isn't big jump between them
iptStartStop=[[1;ipt(1:end-1)] ipt];
MinIPTPoint=CueIndex+ceil(Samplerate*0.15);%The start APA cannot happen before 150ms from the cue. For young it is on average 300ms from cue
% Asymmetry of Anticipatory Postural Adjustment During Gait Initiation by 
% Koichi Hiraoka, Ryota Hatanaka, Yasutaka Nikaido, Yasutomo Jono,  
% Yoshifumi Nomura, Keisuke Tani, Yuta Chujo  

iptStartStop(ipt<=MinIPTPoint,:)=[];
ipt(ipt<=MinIPTPoint)=[];
%If there are several break points, making sure that the first point is
%the real change.
%Angles(2)<0.0001 - making sure that after the break point it is not a flat
%line (rarely happens when the there is a dc change, but it is not a real
%ipt point)
%abs(y_2-y_1)< range(Signal)/20 - the two fit lines should roughly continue
%one eachother and a big jump between then is usualy a dc jump which does
%not relate to the actual ipt point
if length(ipt)>1
    Angles=abs(diff(Signal(iptStartStop),[],2)./diff(iptStartStop,[],2));
    y_1 = polyval(polyfit([1:ipt(1)],Signal([1:ipt(1)])',1),ipt(1));
    y_2 = polyval(polyfit([ipt(1):ipt(2)],Signal([ipt(1):ipt(2)])',1),ipt(1));
    if Angles(2)<0.0001 && abs(y_2-y_1)< range(Signal)/20
        ipt(1)=[];
    end
end
%%%
DiffSig=abs(diff(Signal));
% SlopeIPT=abs(diff(Signal(ipt(1:2)))/diff(ipt(1:2)));
All_IPT=ipt;%Used for plotting;
ipt=ipt(1);

SlopeIPT=DiffSig(ipt);
OrigIPT=ipt;
if ~isempty(ipt)
    %Correcting the start point to loacal minima if exist in very close
    %proximity
    Bound=0.15*Samplerate;%Number of samples porior to the start point to search for local minima
    [MAX,~]=find(imregionalmax(Signal));
    [MIN,~]=find(imregionalmin(Signal));
    
    LocalExtreme=sort([MAX; MIN]);
    LocalExtreme(LocalExtreme>ipt)=[];
    LocalExtreme(LocalExtreme<ipt-Bound)=[];
    if isempty(LocalExtreme)
        iptExtreme=0;%No extreme point in the bound range
    else
        iptExtreme=LocalExtreme(end);
    end
%     SlopeIPT=DiffSig(ipt);z
    for Index=ipt-1:-1:ipt-Bound
        if (abs(DiffSig(Index))<abs(SlopeIPT/5)) % stopping at slope smaller than 20% of the ipt change point
            %stopping in cases of constant change (drift or plato) or a
            %very small angle change(compared to the original ipt slope)
            if 0.9*abs(DiffSig(Index)) < (abs(DiffSig(Index-1))) || DiffSig(Index)< 0.1*SlopeIPT
                ipt=Index;%In order not to be on 
                break;
            end
        end
    end
    ipt=max([iptExtreme ipt]);

    if ipt <CueIndex+ceil(Samplerate*0.15)
        ipt=OrigIPT;
    end
    APA_Start=Time(ipt);
    if Plot
        h=figure;
        hold on;
        plot(Time,Signal,'linewidth',1.3);
        axis('tight');
        Axis=axis;        
        title('Linear ipt')
        All_IPT=[1;All_IPT;length(Signal)];
        for DegreeIndex=1:length(All_IPT)-1
            T=Time(All_IPT(DegreeIndex):All_IPT(DegreeIndex+1));
            A=polyfit(1:length(Signal(All_IPT(DegreeIndex):All_IPT(DegreeIndex+1))),Signal(All_IPT(DegreeIndex):All_IPT(DegreeIndex+1))',1);
%             plot(All_IPT(DegreeIndex):All_IPT(DegreeIndex+1),Signal(All_IPT(DegreeIndex):All_IPT(DegreeIndex+1)),'linewidth',2);
            yData = polyval(A,[1:length(T)]);hold on;
            plot(T,yData,'r','linewidth',1.2);
        end
        for DegreeIndex=1:length(All_IPT)-2
            line([Time(All_IPT(DegreeIndex)) Time(All_IPT(DegreeIndex))],[Axis(3) 0.8*Axis(4)],'color','k');            
        end
        line([0 0],[Axis(3) 0.8*Axis(4)],'color','k');
        text(0-10,0.9*Axis(4),'Auditory cue','FontSize',18);
        text(Time(All_IPT(2))-10,0.9*Axis(4),'First change','FontSize',18);
        
        plot(APA_Start,Signal(ipt),'ko');
                title('APA start detection','fontsize',28);
        xlabel('Time[milliseconds]','fontsize',28);
        ylabel('Acceleration[g]','fontsize',28);
        a = get(gca,'XTickLabel');
        set(gca,'XTickLabel',a,'fontsize',18)
    end
else
    APA_Start=nan;
end
