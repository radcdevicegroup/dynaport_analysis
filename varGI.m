function [GIdata, GIflags, visualGIflag]=varGI(DataFile,PlotFlag,signal)
% clear; close all; warning off;
k=find(signal(:,7));
n=numel(k);
if n>2
    [GI_Parameters] = f_GaitInitiation(DataFile,PlotFlag);
    GIdata = [GI_Parameters.Time_to_APA, GI_Parameters.APA_duration, GI_Parameters.APA_to_HeelStrike, GI_Parameters.Time_to_ToeOff, GI_Parameters.Swing_time];
    GIvalidflag = 0;
    GIflags = [GIvalidflag, GI_Parameters.InitiationStep_TO_Not_Detected, GI_Parameters.Swing_ErrorFlag, GI_Parameters.APA_Duration_ErrorFlag, GI_Parameters.MainErrorFlag];
    
else
    GIdata = ["NaN", "NaN", "NaN", "NaN", "NaN"];
    GIvalidflag = 1;
    GIflags = [GIvalidflag, 0, 0, 0, 0];
end
visualGIflag=0;
%% All flags in GIflags that detect issues after calculation should result in no presentation of the GI data. 4/16/2021 CTZ
if sum(GIflags)>0
    GIdata = ["NaN", "NaN", "NaN", "NaN", "NaN"];
end
    
end


