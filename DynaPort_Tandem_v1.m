function varargout = DynaPort_Tandem_v1(varargin)
% DYNAPORT_TANDEM_V1 MATLAB code for DynaPort_Tandem_v1.fig
%      DYNAPORT_TANDEM_V1, by itself, creates a new DYNAPORT_TANDEM_V1 or raises the existing
%      singleton*.
%
%      H = DYNAPORT_TANDEM_V1 returns the handle to a new DYNAPORT_TANDEM_V1 or the handle to
%      the existing singleton*.
%
%      DYNAPORT_TANDEM_V1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DYNAPORT_TANDEM_V1.M with the given input arguments.
%
%      DYNAPORT_TANDEM_V1('Property','Value',...) creates a new DYNAPORT_TANDEM_V1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DynaPort_Tandem_v1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DynaPort_Tandem_v1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DynaPort_Tandem_v1

% Last Modified by GUIDE v2.5 29-Mar-2021 14:32:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DynaPort_Tandem_v1_OpeningFcn, ...
                   'gui_OutputFcn',  @DynaPort_Tandem_v1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DynaPort_Tandem_v1 is made visible.
function DynaPort_Tandem_v1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DynaPort_Tandem_v1 (see VARARGIN)

% Choose default command line output for DynaPort_Tandem_v1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DynaPort_Tandem_v1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DynaPort_Tandem_v1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in pendingFiles.
function pendingFiles_Callback(hObject, eventdata, handles)
% hObject    handle to pendingFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pendingFiles contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pendingFiles


% --- Executes during object creation, after setting all properties.
function pendingFiles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pendingFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in addButton.
function addButton_Callback(hObject, eventdata, handles)
% hObject    handle to addButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global prev_path;

[infilename, inpathname] = uigetfile('*TANDEM.txt', 'Multiselect', 'on');
prev_str = get(handles.pendingFiles, 'String');


% if prev_str contains only one line of text, it may not be a cell; make it
% a cell
if ~iscell(prev_str)
    temptext = prev_str;
    
    prev_str = cell(1);
    prev_str{1} = temptext;
end


% may need to create it as cell
if ~exist('prev_path', 'var')
    prev_path = cell(1);
elseif ~iscell(prev_path)
    temptext = prev_path;
    
    prev_path = cell(1);
    prev_path{1} = temptext;
end


% if infilename contains only a single filename, it may not be a cell; make
% it a cell if it contains a single filename
if ~iscell(infilename) && ~isnumeric(infilename)
    temptext = infilename;
    
    infilename = cell(1);
    infilename{1} = temptext;
end

% if a file was selected (ie, user did not press cancel button on get file
% ui) then add file to list. otherwise do nothing.
if iscell(infilename)

    % determine number of genuine files already displayed by checking length of
    % prev_str, keeping in mind that if length = 1, the single line could
    % contain either the default "no file selected" message or a genuine
    % filename
    if length(prev_str) > 1
        curlength = length(prev_str);
    else
        curlength = 0;
    end
    
    for i = 1 : length(infilename)
        prev_str{i+curlength} = infilename{i};
        prev_path{i+curlength} = inpathname;
    end

     % Add composed list to listbox
    set(handles.pendingFiles, 'String', prev_str, 'Value', length(prev_str));
    
    if curlength == 0 %this means "if there was not already a valid file in the list"
        loadNewData(prev_path{1}, prev_str{1});
    end
    
    %Make selected file the first file
    set(handles.pendingFiles, 'Value', 1);
end


% --- Executes on button press in removeButton.
function removeButton_Callback(hObject, eventdata, handles)
% hObject    handle to removeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global prev_path;

prev_str = get(handles.pendingFiles, 'String');
toremove = get(handles.pendingFiles, 'Value');
toremove = sort(toremove, 'descend');

if ~iscell(prev_str)
    temptext = prev_str;
    
    prev_str = cell(1);
    prev_str{1} = temptext;
end

if ~iscell(prev_path)
    temptext = prev_path;
    
    prev_path = cell(1);
    prev_path{1} = temptext;
end

if length(toremove) == length(prev_path)
    removeall = true;
else
    removeall = false;
end

% %loop in case more than one file selected
for i = length(toremove):-1:1
        
    if ( strcmp('.txt', prev_str{i}(length(prev_str{i})-3:length(prev_str{i}))) )
      
        %If first file was removed, need to load new data
        if toremove(i) == 1 && removeall == false 
            prev_str(toremove(i)) = [];
            prev_path(toremove(i)) = [];
            loadNullData();
            loadNewData(prev_path{1}, prev_str{1});
        %If all files removed    
        elseif toremove(i) == 1 && removeall == true
            prev_str{1} = 'No files selected.';
            clearvars -global prev_path;
            loadNullData();
            drawPlots(0);
        elseif toremove(i) ~= 1
            prev_str(toremove(i)) = [];
            prev_path(toremove(i)) = [];
        end
          
    end
end

if toremove(1) > length(prev_str)
    selection = toremove(1)-1;
else
    selection = toremove(1);
end

set(handles.pendingFiles, 'String', prev_str, 'Value', selection);



function id_Callback(hObject, eventdata, handles)
% hObject    handle to id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of id as text
%        str2double(get(hObject,'String')) returns contents of id as a double


% --- Executes during object creation, after setting all properties.
function id_CreateFcn(hObject, eventdata, handles)
% hObject    handle to id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function manMissteps_Callback(hObject, eventdata, handles)
% hObject    handle to manMissteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of manMissteps as text
%        str2double(get(hObject,'String')) returns contents of manMissteps as a double


% --- Executes during object creation, after setting all properties.
function manMissteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to manMissteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global prev_path;
global DATA;
global failFlag;


if length(get(handles.id, 'String')) ~= 3
    errordlg('Specify your 3-digit ID before saving.','modal')
else
    prev_str = get(handles.pendingFiles, 'String');
  
    if ~iscell(prev_str)
        temptext = prev_str;
        
        prev_str = cell(1);
        prev_str{1} = temptext;
    end
    
    % may need to create it as cell
    if ~exist('prev_path', 'var')
        prev_path = cell(1);
    elseif ~iscell(prev_path)
        temptext = prev_path;
        
        prev_path = cell(1);
        prev_path{1} = temptext;
    end
    
    newdir = [prev_path{1} '/TANDEM_analysis'];
    if exist(newdir, 'dir') ~= 7
        mkdir(newdir);
    end
    
    
    filename = [newdir '/' prev_str{1}];
    filename = filename(1:length(filename)-4);
    filename = [filename '_analysis.txt'];
    fpout = fopen(filename, 'w');
    
    fprintf(fpout, 'File: %s\r\n', prev_str{1});
    fprintf(fpout, 'GUI Version 1.1\r\n');
    dtStr = ['Analyzed on: ' datestr(datetime('now')) '\r\n'];
    fprintf(fpout, dtStr);
    idstr = ['Performing analysis: ' get(handles.id, 'String') '\r\n'];
    fprintf(fpout, idstr);
    fprintf(fpout, '\r\n');
    
    if failFlag==1
        fprintf(fpout, 'Failed Analysis');
    else
    
        fprintf(fpout, 'Total Time from Markers (s): %g\r\n', DATA.totalTime);
        fprintf(fpout, 'Performance Time (s): %g\r\n', DATA.perfTime);
        fprintf(fpout, 'Number of Steps: %g\r\n',DATA.nStep);
        fprintf(fpout, 'Mean Step Time (s): %g\r\n',DATA.stepTime);
        fprintf(fpout, 'Step Time CV (%): %g\r\n', DATA.stepCV);
        fprintf(fpout, 'Sum Vector Magnitude (g/s): %g\r\n', DATA.svm);
        fprintf(fpout, 'RMS of ML acc (g/s): %g\r\n',DATA.rmsML);
        fprintf(fpout, 'Standard Deviation of ML acc (g): %g\r\n',DATA.stdML);
        fprintf(fpout, 'Mean Velocity in ML (g*s): %g\r\n',DATA.mvML);
        fprintf(fpout, 'Mean Velocity in AP (g*s): %g\r\n',DATA.mvAP);
        fprintf(fpout, 'Mean 2D Velocity in ML/AP (g*s): %g\r\n',DATA.mv2D);
        fprintf(fpout, 'High to Low Frequency Band Ratio: %g\r\n',DATA.freqRatio);
        fprintf(fpout, 'Sample Entropy in ML: %g\r\n',DATA.entropyML);
        fprintf(fpout, 'Sample Entropy in AP: %g\r\n',DATA.entropyAP);
        fprintf(fpout, 'Step Regularity: %g\r\n',DATA.stpReg);
        fprintf(fpout, 'Stride Regularity: %g\r\n', DATA.strReg);
        fprintf(fpout, 'Step Symmetry: %g\r\n', DATA.stpSym);
        fprintf(fpout, 'Step Indices: %g\r\n\r\n', DATA.stepInd');
        
    end

    fclose(fpout);
    
    % remove file from pendingFileList
    % that should take care of loading in the next data as well
    set(handles.pendingFiles, 'Value', 1);
    h = findobj('Style', 'pushbutton', 'Tag', 'removeButton');
    removeButton_Callback(h, eventdata, handles);
end


function loadNullData()
%Clears all variables
clear -global DATA;
global failFlag;
failFlag = 0;

drawPlots(0);

function loadNewData(dataPath, file)

loadNullData;

global DATA;
global failFlag;
global acc;
global indS;

fs = 100;
signal = importdata([dataPath, file]);
indS = find(signal(:,7),1);
if isempty(indS), indS = 5*fs; end
indE = find(signal(:,7),1,'last');
if isempty(indE) || indE==indS
    indE = length(signal)-(3*fs); 
end

%Make Sure test duration is at least 5 seconds
if indE-indS < 5*fs
    failFlag = 1;
    drawPlots(0);
    errordlg('Performance too short, please fail analysis','modal');
    
else
    %Add buffer to start marker
    
    buffAdd = 0;
    if indS > fs
        indS = indS - fs;
        buffAdd = buffAdd + fs;
    else
        buffAdd = buffAdd + indS;
        indS = 1;
    end
    %Use orientation correction algorithm based on orientation prior to start (quiet standing)
    if indS > fs
        rngOR = indS-fs:indS;
    else
        rngOR = 1:fs;
        
    end
    %Use orientation correction algorithm based on orientation prior to start
    avgOr = mean(signal(rngOR,:));
    acc = CorrectAlignment(signal(indS:indE, :), avgOr);
    acc = real(acc);
%     acc(:,1:3) = acc(:,1:3)*9.807;       %multiply by gravity
    v = acc(:,1); ml = acc(:,2); ap = acc(:,3);
    yaw = acc(:,4); pitch = acc(:,5); roll = acc(:,6);
    totalTime = length(v)/(fs);
    timeVec = linspace(0,totalTime,length(v))';

    %calibration- Removing DC offset 
    v_norm=v-mean(v);ml_norm=ml-mean(ml);ap_norm=ap-mean(ap);

    %AP filtering- filter the AP signal in the gait frequencies band  
    fc1=0.25;fc2=2; %the cut off frequencies( on the time domain [0.5-4]s)
    [numer,denom] = butter(1,[fc1 fc2]/(fs/2),'bandpass');
    apFilt = filtfilt(numer,denom,ap); %the filterd signal

    %Calculate Frequency Measures in ML ACC
    %%frequncy measures
    f2=8; f0=0.5/2; f1=2.5/2;
    PSD_HF_LF=[];areaLF=[];areaHF=[];PS=[];

    fftLength = 2048; wn=2*fs;  

    [PS,~] = pwelch(ml_norm./std(ml), wn,[],fftLength,fs); %power spectral density 
    Y=PS;
    Fvec_half = fs/2*linspace(0,1,fftLength/2);
    singlesideY=(Y(1:fftLength/2));

    f0ix=find(Fvec_half<=f0);f0ix=f0ix(end);
    f1ix=find(Fvec_half<=f1);f1ix=f1ix(end);
    f2ix=find(Fvec_half<=f2);f2ix=f2ix(end);

    LFAMP=singlesideY(f0ix:f1ix);L_FREQ=Fvec_half(f0ix:f1ix); %PSD of the low frequency bands
    HFAMP=(singlesideY(f1ix:f2ix));H_FREQ=Fvec_half(f1ix:f2ix);%PSD of the high frequency bands

    %For plotting
    fVec = Fvec_half(f0ix:f2ix);
    ssY = singlesideY(f0ix:f2ix);

    %calculating the area under the curves
    areaLF=trapz(L_FREQ,LFAMP); 
    areaHF=trapz(H_FREQ,HFAMP);

    PSD_HF_LF=[PSD_HF_LF areaHF/areaLF];

    %%SVM
    %To assess overall movement during the TW
    %we are calculating the signal vector magnitude of the acceleration signal from three axes
    %normalized by the test duration 
    SVM=sqrt(sum( v_norm.^2 + ml_norm.^2 + ap_norm.^2) )/totalTime;
    RMS_ML=rms(ml_norm)/totalTime;

    %mean velocity
    mvAP = abs(trapz(timeVec, ap_norm));
    mvML = abs(trapz(timeVec, ml_norm));
    mv2D = sqrt(mvML ^ 2 + mvAP ^ 2); 

    %sample entropy 
    %To quantify regularity and complexity in the time series of the TW,
    %we estimating the sample entropy from AP and ML axis
    M=3;r=0.07;
    [e_ML,A_ML,B_ML]=sampenc(ml_norm,M,r);%sample entropy ML
    [e_AP,A_AP,B_AP]=sampenc(ap_norm,M,r);%sample entropy AP

    %detecting steps and calculating the number of steps, %CV, and step time. 
    [Nstep,step_time,CV, stepIX]=tandem_step_detection(apFilt,timeVec,ml_norm,ap_norm,fs);
    if isempty(stepIX)
        drawplots(0);
        errordlg('No steps detected, fail analysis');
    end

    %calculating the variability from the ML axis
    STD_ML=std(ml_norm);

    %Calculate Regularity from Autocorrelation
    vert = v_norm./std(v);
    vert = vert(stepIX(1):stepIX(end));
    [c, lags] = xcov(vert, 'unbiased');
    d0 = find(lags==0);
    c = c(d0:end);
    lags = lags(d0:end);
    
    if max(c)>1
        c = c./max(c);
    end

    thresh = 0.05;
    [Ad, ind] = findpeaks(c, 'MinPeakProminence', thresh, 'MinPeakHeight', 0.1);
    d=lags(ind);
    
    if length(d)<2
        [Ad, ind] = findpeaks(c);
        rem = find(Ad<0);
        Ad(rem) = [];
        ind(rem) = [];
        d = lags(ind);
        
        %If there are no peaks in c, something is wrong with the signal
        if isempty(d)
            %Find new walking segment using vertical acc
            [~, tempInd] = findpeaks(vert, 'MinPeakProminence', thresh, 'MinPeakHeight', 0);
            vInd = (tempInd(1):tempInd(end)) + walkSeg(ii,1);
            vNew = acc(vInd,1);
            vNew = (vNew - mean(vNew)) ./ std(vNew);
            [c, lags] = xcov(vNew, 'unbiased');
            d0 = find(lags==0);
            c = c(d0:end);
            lags = lags(d0:end);
            if max(c)>1, c = c./max(c); end
            [Ad, ind] = findpeaks(c, 'MinPeakProminence', thresh, 'MinPeakHeight', 0.1);
            d=lags(ind);
        end
    end

    
    d1=d(1); Ad1=Ad(1);
    if length(d)>=2
        d2=d(2); Ad2=Ad(2);
    else
        d2 = length(c);
        Ad2 = c(d2);
    end
        
    if (d2-d1) < d1/2 && length(d) > 3
        Ad1 = max([Ad1, Ad2]);
        d1 = d(find(Ad==Ad1));
        d2 = d(3);
        Ad2 = c(d(3));
    end
    
    DATA.stepInd = stepIX + indS - 1;
    
    DATA.stpReg = Ad1;
    DATA.strReg = Ad2;
    DATA.stpSym = Ad1/Ad2;
    
    DATA.perfTime = (stepIX(end) - stepIX(1))/fs;
    DATA.totalTime = totalTime - (buffAdd/fs);     %account for buffer
    DATA.nStep = Nstep;
    DATA.stepTime = step_time;
    DATA.stepCV = CV;
    DATA.svm = SVM;
    DATA.rmsML = RMS_ML;
    DATA.stdML = STD_ML;
    DATA.mvML = mvML;
    DATA.mvAP = mvAP;
    DATA.mv2D = mv2D;
    DATA.entropyML = e_ML(end);
    DATA.entropyAP = e_AP(end);
    DATA.freqRatio = PSD_HF_LF;

    drawPlots(fVec, ssY,apFilt, stepIX)
end


function drawPlots(fVec, ssY, apFilt, stepIX)
fs = 100;
fMin = 0.25; fMax = 8; f1=2.5/2;

if nargin>1
    %Plot AP Acc with steps
    totalTime = length(apFilt)/(fs);
    timeVec = linspace(0,totalTime,length(apFilt))';
    hAP = findobj('Type', 'axes', 'Tag', 'apPlot');
    cla(hAP);
    axes(hAP);
    line(timeVec, -apFilt, 'color', 'k')
    line(timeVec(stepIX), -apFilt(stepIX), 'Marker', '*', 'Color' , 'g', 'LineStyle' , 'none');
    set(hAP, 'XLim', [0 totalTime]);
    
    %Plot Freq Response   
    f1ix=find(fVec<=f1, 1, 'last');
    hF = findobj('Type', 'axes', 'Tag', 'freqPlot');
    cla(hF);
    axes(hF);
    line(fVec, ssY, 'color', 'k');
    line([fVec(f1ix),fVec(f1ix)], [0,ssY(f1ix)], 'color', 'r');
    set(hF, 'XLim', [fMin fMax]);
    
else % for the case that it's null data (there is no valid file in the pending file list)
    
    hAP = findobj('Type', 'axes', 'Tag', 'apPlot');
    cla(hAP);
    axes(hAP);
    set(hAP, 'XLim', [0 30]);
    
    hF = findobj('Type', 'axes', 'Tag', 'freqPlot');
    cla(hF);
    axes(hF);
    set(hF, 'XLim', [fMin fMax]);
    
    
end


% --- Executes on button press in failPerformance.
function failPerformance_Callback(hObject, eventdata, handles)
% hObject    handle to failPerformance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global failFlag;

failFlag = 1;
h = findobj('Style', 'pushbutton', 'Tag', 'saveButton');
saveButton_Callback(h, eventdata, handles);


% --- Executes on button press in resetButton.
function resetButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global prev_path;
prev_str = get(handles.pendingFiles, 'String');
% if prev_str contains only one line of text, it may not be a cell; make it
% a cell
if ~iscell(prev_str)
    temptext = prev_str;
    
    prev_str = cell(1);
    prev_str{1} = temptext;
end


% may need to create it as cell
if ~exist('prev_path', 'var')
    prev_path = cell(1);
elseif ~iscell(prev_path)
    temptext = prev_path;
    
    prev_path = cell(1);
    prev_path{1} = temptext;
end
loadNewData(prev_path{1}, prev_str{1});




% --- Executes on button press in addStep.
function addStep_Callback(hObject, eventdata, handles)
% hObject    handle to addStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global DATA;
global acc;
global indS;
fs=100;
v = acc(:,1); ml = acc(:,2); ap = acc(:,3);
yaw = acc(:,4); pitch = acc(:,5); roll = acc(:,6);
totalTime = length(v)/(fs);
timeVec = linspace(0,totalTime,length(v))';

%calibration- Removing DC offset 
v_norm=v-mean(v);ml_norm=ml-mean(ml);ap_norm=ap-mean(ap);

%AP filtering- filter the AP signal in the gait frequencies band  
fc1=0.25;fc2=2; %the cut off frequencies( on the time domain [0.5-4]s)
[numer,denom] = butter(1,[fc1 fc2]/(fs/2),'bandpass');
apFilt = filtfilt(numer,denom,ap); %the filterd signal

stepIX = DATA.stepInd;
stepIX = stepIX - indS + 1;

[pks,locs] =findpeaks((-apFilt(1:stepIX)));

if isempty(pks)
    errordlg('No Step Detected','modal')
else
    if length(pks)>1
        [~,temp] = max(pks);
        locs = locs(temp);
    end
    stepIX = [locs; stepIX];
    time_vec=diff(stepIX)*fs;
    step_time=mean(time_vec); % mean step duration
    CV=(std(time_vec)/mean(time_vec))*100; %Coefficient of variation for the step duration

    DATA.stepInd = stepIX + indS -1;
    DATA.perfTime = (stepIX(end) - stepIX(1))/fs;
    DATA.nStep = length(stepIX);
    DATA.stepTime = step_time;
    DATA.stepCV = CV;


    %%For Plotting Also calculate Frequency Data
    %Calculate Frequency Measures in ML ACC
    %%frequncy measures
    f2=8; f0=0.5/2; f1=2.5/2;
    fftLength = 2048; wn=2*fs;  

    [PS,~] = pwelch(ml_norm./std(ml), wn,[],fftLength,fs); %power spectral density 
    Y=PS;
    Fvec_half = fs/2*linspace(0,1,fftLength/2);
    singlesideY=(Y(1:fftLength/2));

    f0ix=find(Fvec_half<=f0);f0ix=f0ix(end);
    f1ix=find(Fvec_half<=f1);f1ix=f1ix(end);
    f2ix=find(Fvec_half<=f2);f2ix=f2ix(end);

    %For plotting
    fVec = Fvec_half(f0ix:f2ix);
    ssY = singlesideY(f0ix:f2ix);

    drawPlots(fVec, ssY,apFilt, stepIX)
end


% --- Executes on button press in removeStep.
function removeStep_Callback(hObject, eventdata, handles)
% hObject    handle to removeStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global DATA;
global acc;
global indS;


%Remove First Step
DATA.stepInd(1) = [];

%This is needed for plotting
fs = 100;
v = acc(:,1); ml = acc(:,2); ap = acc(:,3);
totalTime = length(v)/(fs);
timeVec = linspace(0,totalTime,length(v))';



%AP filtering- filter the AP signal in the gait frequencies band  
fc1=0.25;fc2=2; %the cut off frequencies( on the time domain [0.5-4]s)
[numer,denom] = butter(1,[fc1 fc2]/(fs/2),'bandpass');
apFilt = filtfilt(numer,denom,ap); %the filterd signal


stepIX = DATA.stepInd;
stepIX = stepIX - indS + 1;

ml_norm=ml-mean(ml);

%%For Plotting Also calculate Frequency Data
%Calculate Frequency Measures in ML ACC
%%frequncy measures
f2=8; f0=0.5/2; f1=2.5/2;
fftLength = 2048; wn=2*fs;  

[PS,~] = pwelch(ml_norm./std(ml), wn,[],fftLength,fs); %power spectral density 
Y=PS;
Fvec_half = fs/2*linspace(0,1,fftLength/2);
singlesideY=(Y(1:fftLength/2));

f0ix=find(Fvec_half<=f0);f0ix=f0ix(end);
f1ix=find(Fvec_half<=f1);f1ix=f1ix(end);
f2ix=find(Fvec_half<=f2);f2ix=f2ix(end);

%For plotting
fVec = Fvec_half(f0ix:f2ix);
ssY = singlesideY(f0ix:f2ix);

drawPlots(fVec, ssY,apFilt, stepIX)