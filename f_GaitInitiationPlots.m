function f_GaitInitiationPlots(DataFile,PlotFlag)
acc_results=[];
% This is the main function to calculate the GI parameters from the signals of
% OPAL, Dynaport and Force-mat.
%
% Input:
% DataFile -     Dynaport text file with full path (e.g. 'C:\project\Rush\07761236_08_20191009_124103_8FT_1.txt')
% PlotFlag -     1=generate a plot with all the parameters, 0 = no plot
%
% Output:
% GI_Parameters - struct containing all the GI results derived from the accl:
%                 timeToAPA [ms]
%                 APAduration [ms]
%                 APAtoHeelStrike [ms]
%                 HeelStrike (from the auditory cue) [ms]
%                 ToeOff (from the auditory cue) [ms]
%                 APAauc (area under the curve in the ml signal for the APA section) [ms]
%                 APAamp (ML peak amplitude in the APA section) [ms]
%
%                 Error flags: (0=no error was detected)
%                 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 InitiationStep_TO_Not_Detected - 1=Toe off of the initiation step was not detected
%                 Swing_ErrorFlag - 1=Swing time is smaller than 100ms
%                 APA_Duration_ErrorFlag - 1=APA duration is smaller than 200ms
%                 Main_ErrorFlag - 1= at least one of the error flags is 1
% Plotting the signals and gait initiation parameters
if PlotFlag==1
try
    %% Pre-process
    % Pre-process the accl data (Opal or Dynaport)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ACCtotal=f_importTXTfile(DataFile);%Importing data
    
    %% Auditory cue
    %Extract the auditory cue index on the annotations data
    %The first "1" - indicating the button press for start
    %The "2" - the cue
    %The second "1" - Stop of the gait initiation trial
    syncIndAcc=find(diff(ACCtotal(:,7))==2);
    TrialEnd=find(diff(ACCtotal(:,7))==1,1,'last');
    
    if TrialEnd>syncIndAcc%In case there is a mark for the end of the recording -  trimming the data after the end signal
        ACCtotal=ACCtotal(1:TrialEnd,:);
    end
    
    % Modifying the dynaport axis direction to match the Opal's (needed for the rest of the analysis in the algorithm)
    v = ACCtotal(:,1)'*9.823 ; ml = -ACCtotal(:,2)'*9.823;  ap = ACCtotal(:,3)'*-9.823;
    OrigV=ACCtotal(:,1)'*9.823;%Storing the original vertical signal for plotting
    % DC removal
    v = v - mean(v); ml = ml - mean(ml); ap = ap - mean(ap);
    %     figure;plot(v);hold on;plot(ap);plot(ml);plot(ACCtotal(:,7))
    fs = 100;
    d_BP = designfilt('bandpassiir','FilterOrder',4,'HalfPowerFrequency1',0.2,'HalfPowerFrequency2',4.5 ,'SampleRate',fs);
    v = filtfilt(d_BP, v); ml = filtfilt(d_BP, ml); ap = filtfilt(d_BP, ap);
    
    %% APA start detection
    StartBias=0.5;%Time in sec before the cue to start searching for the APA start(should be quiet standing)
    Indexes=[syncIndAcc-StartBias*fs:syncIndAcc+1.2*fs];%Looking at an area of StartBias sec prior to the cue (quiet standing) up
    %to 1.2 sec after. APA start should be 200-500ms after the cue, but in
    %rare cases might get close to 1 sec
    
    Time=1000*([0:length(ml)-1]/fs-syncIndAcc/fs);%Creating time vector in milliseconds. The auditory cue is at set to be at Time =0
    Time=Time(Indexes);%Trimming the time vector to 0.5 prior to the cue and 1.2sec after it
    %Searching in each acceleration axis for the beginning of the movement
    %APA start is the earliest movement detected
    PlotAPAStart=0;%0=no plots, 1= plot the f_CalcAPAStart graph
    APA_Start_ml=nan;APA_Start_ap=nan;APA_Start_v=nan;
    try APA_Start_ml=f_CalcAPAStart(ml(Indexes),Time,fs,PlotAPAStart);end
    try APA_Start_ap=f_CalcAPAStart(ap(Indexes),Time,fs,PlotAPAStart);end
    try APA_Start_v=f_CalcAPAStart(v(Indexes),Time,fs,PlotAPAStart);end
    APA_Start_ml_ap_v=nanmin([APA_Start_ml APA_Start_ap APA_Start_v]);%The time APA started
    
    %% APA end
    %Detecting AP peak - close to the HS of the leading leg and TO of trailing leg (first step after the initiation step)
    %'MinPeakDistance',fs/4 - gait peaks have min distance
    %'MinPeakHeight',0 - peak height should be above 0(removing small
    %                    negative peaks that might exist due to noise)
    %'MinPeakProminence' - for removal of very small local peaks
    
    AP_Prominance_Threshold=range(prctile(ap,[10,90]))/100;%removing very small local peaks
    [AP_pks,AP_locs] = findpeaks(ap,'MinPeakDistance',fs/3,'MinPeakHeight',0,'MinPeakProminence',AP_Prominance_Threshold);
    %Removing small peaks (usually appears at the quiet standing/turns)
    while prctile(AP_pks,95)>4*prctile(AP_pks,2)
        AP_locs(AP_pks<=prctile(AP_pks,5))=[];
        AP_pks(AP_pks<=prctile(AP_pks,5))=[];
    end
    AP_pks(AP_locs<syncIndAcc)=[];%removing peaks before the sync cue signal
    AP_locs(AP_locs<syncIndAcc)=[];%removing peaks before the sync cue signal
    %figure;plot(ap);hold on;stem(AP_locs,AP_pks)
    
    %Searching for vertical peaks. They are used for removing wrong ap peaks and find the
    %initial step
    V_Prominance_Threshold=range(prctile(v,[10,90]))/100;
    [V_pks,V_locs] = findpeaks(v,'MinPeakDistance',fs/3,'MinPeakHeight',0,'MinPeakProminence',V_Prominance_Threshold);
    
    while prctile(V_pks,95)>4*prctile(V_pks,5)
        V_locs(V_pks<=prctile(V_pks,5))=[];
        V_pks(V_pks<=prctile(V_pks,5))=[];
    end
    V_pks(V_locs<syncIndAcc)=[];%removing peaks before the sync cue signal. Could appear if there is movement at the beginning of the recording
    V_locs(V_locs<syncIndAcc)=[];%removing peaks before the sync cue signal
    %     figure;plot(v);hold on;stem(V_locs,V_pks);
    
    Time=1000*([0:length(ml)-1]/fs-syncIndAcc/fs);%0=syncIndAcc
    PlotPaper=0;
    if PlotPaper %Plots for debug/paper
        h=figure;plot(Time,v);hold on;plot(Time,ap,'-.','linewidth',1.1);
        plot(Time(AP_locs),AP_pks,'vk');plot(Time(V_locs),V_pks,'^r');
        axis('tight');Axis=axis;Axis(1)=-500; Axis(2)=7000;axis(Axis);
        xlabel('Time [milliseconds]');ylabel('Acceleration [g]');
        legend('Vertical','Anterior-posterior','Location','northeast','autoupdate','off')
        title('Detection of step location in the acceleration data');
        set(gca,'fontsize',20);
        Axis=axis;Axis(1)=-500;axis(Axis);%Change the graph time to start from 0.5 sec before the cue
        line([0 0],[Axis(3) 0.55*(Axis(4)-Axis(3))+Axis(3)],'color','k');text(-0.5,0.5*Axis(4),'Auditory Cue','fontsize',14);
    end
    
    %The AP peak cannot appear before the first vertical peak (probably
    %peak from the APA)
    AP_pks(AP_locs<V_locs(1))=[];
    AP_locs(AP_locs<V_locs(1))=[];
    
    %There shouldn't be more than 1 Vertical peak prior to the first one in the AP
    V_PreAP=find(V_locs<AP_locs(1));
    V_locs(V_PreAP(1:end-1))=[];
    V_pks(V_PreAP(1:end-1))=[];
    
    %Adaptation to 8ft/32ft walks
    V_locs=V_locs(1:4);%Taking only the first 4 steps
    V_pks=V_pks(1:4);%Taking only the first 4 steps
    AP_locs=AP_locs(2:4);
    AP_pks=AP_pks(2:4);
    
    StepTime=median(diff(V_locs));%Used for the initiation step search window
    %Calculating thresholds for quiet standing detection
    StepAMP_AP=range(ap(AP_locs));%Used for detecting quit standing
    StepSTD_AP=std(ap(AP_locs));%Used for quiet standing detection
    StepAMP_ML=range(ml(AP_locs));%Used for detecting quit standing
    StepSTD_ML=std(ml(AP_locs));%Used for quiet standing detection
    
    
    %Removing peaks that are too close to the sync signal (800ms)
    %The threshold of 800ms from the cue was chosen from:
    %Asymmetry of Anticipatory Postural Adjustment During Gait Initiation
    %Koichi Hiraoka, Ryota Hatanaka, Yasutaka Nikaido, Yasutomo Jono,Yoshifumi Nomura, Keisuke Tani, Yuta Chujo
    %By summing S1+S2+S3
    try
        AP_pks(AP_locs<syncIndAcc+0.8*fs)=[];%Explanation about the 800ms from cue is below
        AP_locs(AP_locs<syncIndAcc+0.8*fs)=[];
    end
    
    %Last validation that the AP peak is from gait
    %AP and V peaks should be close during gait
    PeaksDistance=min(abs(V_locs-AP_locs(1)));
    if PeaksDistance>0.5*StepTime
        %Probably peak of AP during the APA phase
        AP_locs(1)=[];
    end
    
    %Searching for peak close to location of one step prior to the AP TO
    %trailing leg(TO of the leading leg)
    V_TO_LeadingAreaEnd=V_locs(1)-0.5*StepTime;
    V_TO_LeadingAreaStart=V_locs(1)-1.5*StepTime;
    V_FirstStep=V_locs(1);
    clear('h_Detection_Acc');
    
    %Searching for the initiation step peak at the vertical axis.
    [V_pks,V_locs] = findpeaks(v,'MinPeakDistance',fs/4);%,'MinPeakProminence',StepAMP/10);%MinPeakHeight - preventing very small peaks from entering
    All_V_locs=V_locs;%Keeping all peak locations for HS detection of the swing leg
    V_pks(V_locs>V_TO_LeadingAreaEnd)=[];V_pks(V_locs<V_TO_LeadingAreaStart)=[];
    V_locs(V_locs>V_TO_LeadingAreaEnd)=[];V_locs(V_locs<V_TO_LeadingAreaStart)=[];
    [Val,Ind]=min(abs(V_locs-V_TO_LeadingAreaEnd));
    
    %Checking if peaks were found. If more than one is found then the last
    %one is chosen. If no peak was found then a flag of no TO detected is
    %raised
    InitiationStep_TO_Not_Detected=0;%0=detected, 1= not detected
    if Ind>=1
        V_Initial_Peak_Locs=V_locs(Ind);
        V_Initial_Peak_AMP=v(V_Initial_Peak_Locs);
    else
        %no peak was found
        V_Initial_Peak_Locs=nan;%No peaks before the AP_TO_LeadingArea
        V_Initial_Peak_AMP=nan;
        InitiationStep_TO_Not_Detected=1;
    end
    
    %Swing leg Heel strike detection. Should be located at the first step
    %when the vertical axis crosses threshold of ~20% from the peak
    GaitPeak=V_FirstStep;
    GaitPeakAMP=v(GaitPeak);
    v_crossing20=find(diff(sign(v-0.2*GaitPeakAMP))==2);
    v_crossing20(v_crossing20>GaitPeak)=[];
    HS_Location20=v_crossing20(end)-syncIndAcc;
    HS_Location20_Time=1000*HS_Location20/fs;%Time is milliseconds from the cue
    %%%
    
    %%%
    %Initiation leg TO detection
    Treshold=0;
    CrossUp=find(diff(sign(v-Treshold*V_Initial_Peak_AMP))==2);
    InitiationStep_TO_Not_Detected=0;%Flag for identifying problem with TO of the initiation step.
    try
        V_Leading_TO=CrossUp(find(CrossUp<V_Initial_Peak_Locs,1,'last'));
    end
    if isempty(V_Leading_TO)
        V_Leading_TO=-999;
    end
    if V_Leading_TO==-999 || V_TO_LeadingAreaEnd-V_Leading_TO>0.75*fs
        %         V_Leading_TO=ML_Leading_TO;%Replacing the TO point to the one detected by ML
        V_Leading_TO=[];%Replacing the TO point to the one detected by ML
        InitiationStep_TO_Not_Detected=1;
    end
    %Sometimes there are 2 close vertical peaks, causing the vertical signal
    %not to cross Zero. In that is the case taking the min point instead of the zero
    %crossing
    [V_pksMin,V_locsMin] = findpeaks(-v,'MinPeakDistance',fs/5);
    if ~isempty(find(V_locsMin>V_Leading_TO & V_locsMin<V_Initial_Peak_Locs))
        V_Leading_TO=V_locsMin(find(V_locsMin>V_Leading_TO & V_locsMin<V_Initial_Peak_Locs,1,'last'));
    end
    Time=1000*([0:length(ml)-1]-syncIndAcc)/fs;%0=syncIndAcc
    V_Leading_TO_Time=Time(V_Leading_TO);
catch Err
    display(DataFile);
    display(Err);
end
%%%

%APA duration
ACC_APA_Duration=V_Leading_TO_Time-APA_Start_ml_ap_v;
Time=1000*([0:length(ml)-1]/fs-syncIndAcc/fs);%0=syncIndAcc

%Adding error flags
Swing_ErrorFlag=0;%0=no error, 1= error
APA_Duration_ErrorFlag=0;%0=no error, 1= error
if HS_Location20_Time-V_Leading_TO_Time<100%Swing cannot be smaller than 100ms
    Swing_ErrorFlag=1;
end
if ACC_APA_Duration<200%APA duration cannot be smaller than 200ms
    APA_Duration_ErrorFlag=1;
end
Main_ErrorFlag=0;%0=no error, 1= error
if InitiationStep_TO_Not_Detected>0 || Swing_ErrorFlag>0 || APA_Duration_ErrorFlag>0
    Main_ErrorFlag=1;
end

%Test if there was quiet standing prior to the que
StandingWinAMP_AP=range(abs(ap(syncIndAcc-fs:syncIndAcc)));
StandingWinSTD_AP=std(ap(syncIndAcc-fs:syncIndAcc));
StandingWinAMP_ML=range(abs(ml(syncIndAcc-fs:syncIndAcc)));
StandingWinSTD_ML=std(ml(syncIndAcc-fs:syncIndAcc));

if StandingWinAMP_AP>0.1*StepAMP_AP || StandingWinSTD_AP>0.1*StepSTD_AP || StandingWinAMP_ML>0.1*StepAMP_ML || StandingWinSTD_ML>0.1*StepSTD_ML
    QuietStanding=0;%Quiet standing was not detected
else
    QuietStanding=1;%Quiet standing detected
end


    %     h=figure;hold on;
%     hGI=figure('units','normalized','outerposition',[0 0 1 1]);hold on;
    hGI=findobj('Type', 'axes', 'Tag', 'giPlot'); %CTZ added trying to link to GUI
    cla(hGI);
    axes(hGI);
    plot(Time,v,'color',[0 0.2 0.9],'linewidth',1.1); hold on;
    plot(Time,ap,'color',[0.9 0.2 0.2],'linewidth',1.1); hold on;
    plot(Time,ml,'linewidth',1.1); hold on;
    Axis=axis;%Storing the axis limits (used later on for focusing on the gait initation start)
    plot(Time,(OrigV-mean(OrigV))/2-1,'color',[0 0.2 0.9],'linewidth',1.1,'color',[0 0 0]);
    legend('vertical(filtered)','anterior-posterior(filtered)','medial lateral(filtered)','vertical(raw)','AutoUpdate','off');
    xlabel('Time [ms]');ylabel('Acceleration [g]');
    Axis(1)=-500;axis(Axis);%Change the graph time to start from 0.5 sec before the cue
    
    line([0 0],[Axis(3) 0.8*Axis(4)],'color','k');text(5,0.85*Axis(4),'Auditory Cue','fontsize',10);
    line([V_Leading_TO_Time V_Leading_TO_Time],[Axis(3) 0.7*Axis(4)],'color','k');text(V_Leading_TO_Time+10,0.75*Axis(4),{'Initiation leg','Toe off'},'fontsize',10);
    line([APA_Start_ml_ap_v APA_Start_ml_ap_v],[Axis(3) 0.7*Axis(4)],'color','k');text(APA_Start_ml_ap_v+10,0.75*Axis(4),'APA start','fontsize',10);
    line([HS_Location20_Time HS_Location20_Time],[Axis(3) 0.7*Axis(4)],'color','k');text(HS_Location20_Time+10,0.75*Axis(4),{'Initiation leg','Heel strike'},'fontsize',10);
    set(gca,'fontsize',10);
%     hGI.CurrentAxes.XLim=[-0.5, HS_Location20_Time+3*1000*StepTime/fs];
xlim([-0.5, HS_Location20_Time+3*1000*StepTime/fs]);
%     TrialName=strsplit(DataFile,'\');TrialName=TrialName{end};
%     title([{'Acceleration signal during gait initiation trial'}, {['File=' regexprep(TrialName,'_','\\_')]}]);
    StartX=APA_Start_ml_ap_v;EndX=V_Leading_TO_Time;
    StartY=0.45*Axis(4);EndY=0.45*Axis(4);
    line([0 APA_Start_ml_ap_v],[StartY EndY],'color','k');
    line([0.95*APA_Start_ml_ap_v 0.95*APA_Start_ml_ap_v],[StartY EndY],'color','k','marker','>','markerindices',1:floor(StartX));
    text(10,StartY-0.5,[{'Quiet'} ,{'standing'}],'FontSize',10);
    
    line([StartX EndX],[StartY EndY],'color','k');
    line([1.02*StartX 1.03*StartX],[StartY EndY],'color','k','marker','<','markerindices',1:StartY);
    line([0.99*EndX 0.99*EndX],[StartY EndY],'color','k','marker','>','markerindices',1:EndY);
    text(StartX*1.1,StartY-0.25,'APA duration','FontSize',10);
    StartX=V_Leading_TO_Time;EndX=HS_Location20_Time;
    line([StartX EndX],[StartY EndY],'color','k');
    line([1.02*StartX 1.02*StartX],[StartY EndY],'color','k','marker','<','markerindices',1:StartY);
    line([0.99*EndX 0.99*EndX],[StartY EndY],'color','k','marker','>','markerindices',1:EndY);
    text(StartX*1.02,StartY-0.5,[{'Initiation'}, {'step'}],'FontSize',10);
    StartX=HS_Location20_Time;EndX=HS_Location20_Time+2000;
    line([StartX EndX],[StartY EndY],'color','k');
    text(StartX*1.02,StartY-0.25,'Walking','FontSize',10);
%     hGI.CurrentAxes.Position=[0.1300,0.1221,0.7750,0.8029];
end

if PlotFlag==0
    hGI = findobj('Type', 'axes', 'Tag', 'giPlot');
    cla(hGI, 'reset');
    axes(hGI);
    set(hGI, 'XLim', [0 30]);
end

end
