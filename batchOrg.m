batchNum = 1;

p = ['F:\DynaPort_Data\dynaport_batch_processed_' num2str(batchNum) '\'];    

pQ = [p 'qc'];
pS = [p 'segmented'];

mkdir(pQ);
mkdir(pS);

fQ = dir([p, '*.qcd']);
fS = dir([p, '*.txt']);

for ii = 1:length(fQ)
    movefile([p fQ(ii).name], pQ);
end

for jj = 1:length(fS)
    movefile([p fS(jj).name], pS);
end


