function [PSD_HF_LF areaLF areaHF]=PSD_Area(data,fs,f0,f1,f2,p)
%To capture a measure of the postural corrections used to stabilize the body during the performance of the TW, 
%we determined the high to low frequency band ratio from the power spectral density (PSD) from the medio-lateral axis.
%It was calculated as the area under the curve of the power spectral density (PSD)
%in the 1.25-8 Hz band to the 0.25-1.25 Hz band from medio-lateral axis [au].
%% Input 
%1) data- the signal
%2) fs- sample rate
%3) 3 freqencies point:f0,f1,f2
%4) p- if p=1 the function is plotting a figure with the area under the PSD.
%% output 
%1)PSD_HF_LF- the power spectral density ratio between high and low frequencies of ML axis
%2)areaLF- the area under the PSD in the low frequencies (walking frequencies)
%3)areaHF-the area under the PSD in the high frequencies

PSD_HF_LF=[];areaLF=[];areaHF=[];PS=[];
data=(data-mean(data))./std(data); %calibating the data  
L = length(data)  ;              
fftLength = 2048; wn=2*fs;  

[PS,~] = pwelch(data, wn,[],fftLength,fs); %power spectral density 
Y=PS;
Fvec_half = fs/2*linspace(0,1,fftLength/2);
singlesideY=(Y(1:fftLength/2));

f0ix=find(Fvec_half<=f0);f0ix=f0ix(end);
f1ix=find(Fvec_half<=f1);f1ix=f1ix(end);
f2ix=find(Fvec_half<=f2);f2ix=f2ix(end);

LFAMP=singlesideY(f0ix:f1ix);L_FREQ=Fvec_half(f0ix:f1ix); %PSD of the low frequency bands
HFAMP=(singlesideY(f1ix:f2ix));H_FREQ=Fvec_half(f1ix:f2ix);%PSD of the high frequency bands

%calculating the area under the curves
areaLF=trapz(L_FREQ,LFAMP); 
areaHF=trapz(H_FREQ,HFAMP);

PSD_HF_LF=[PSD_HF_LF areaHF/areaLF];

%-------figure
 if(p==1)
 figure();
 p1=plot(L_FREQ,LFAMP,'k','LineWidth',3); 
 hold on; p2=plot(H_FREQ,HFAMP,'k','LineWidth',3); 
 h=area(H_FREQ(H_FREQ<=f2), HFAMP(H_FREQ<=f2));
 h.FaceColor = 'r';
ylim([ 0 0.6])
 h=area(L_FREQ(L_FREQ<=f1), LFAMP(L_FREQ<=f1));
 h.FaceColor = [91, 207, 244] / 255;
 set(gca, 'FontSize', 20)
 xlabel('Frequency [Hz]');ylabel('PSD [(m^2/s^4)/hz]');
 end
 
end