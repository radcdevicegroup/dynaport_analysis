G = 9.807;

fs = 100;
plotFlag = 0;

pTand = 'G:\Kinect_Field\segmented\';
fName = '74343949_02_20200305_142818_TANDEM.txt';

signal = importdata([pTand fName]);
%Find Markers, if none found, use default from QC of 5sec before start and 3sec after end
indS = find(signal(:,7),1);
if isempty(indS), indS = 5*fs; end
indE = find(signal(:,7),1,'last');
if isempty(indE) 
    indE = length(signal)-(3*fs); 
elseif indE==indS
    indE = length(signal)-(3*fs);
end

%Make Sure test duration is at least 5 seconds
if indE-indS > 5*fs
    indS = indS + fs;
    indE = indE - fs;
end

%Use orientation correction algorithm based on orientation prior to start
if indS > fs
    rngOR = indS-fs:indS;
else
    rngOR = 1:fs;
end
avgOr = mean(signal(rngOR,:));
acc = CorrectAlignment(signal(indS:indE, :), avgOr);
acc = real(acc);
% acc(:,1:3) = acc(:,1:3)*G;
v = acc(:,1); ml = acc(:,2); ap = acc(:,3);
yaw = acc(:,4); pitch = acc(:,5); roll = acc(:,6);
totalTime = length(v)/(fs);
timeVec = linspace(0,totalTime,length(v))';

%calibration- Removing DC offset 
v_norm=v-mean(v);ml_norm=ml-mean(ml);ap_norm=ap-mean(ap);

%AP filtering- filter the AP signal in the gait frequencies band  
fc1=0.25;fc2=2; %the cut off frequencies( on the time domain [0.5-4]s)
[numer,denom] = butter(1,[fc1 fc2]/(fs/2),'bandpass');
apFilt = filtfilt(numer,denom,ap); %the filterd signal
mlFilt = filtfilt(numer,denom,ml);

%%frequncy measures
f2=8;f0=0.5;f1=2.5;
[PSD_HF_LF, areaLF, areaHF]=PSD_Area(ml,fs,f0/2,f1/2,f2,plotFlag); %ML

%%SVM
%To assess overall movement during the TW
%we are calculating the signal vector magnitude of the acceleration signal from three axes
%normalized by the test duration 
SVM=sqrt(sum( v_norm.^2 + ml_norm.^2 + ap_norm.^2) )/totalTime;
RMS_ML=rms(ml_norm)/totalTime;

%mean velocity
mvAP = abs(trapz(timeVec, ap_norm));
mvML = abs(trapz(timeVec, ml_norm));
mv2D = sqrt(mvML ^ 2 + mvAP ^ 2); 

%sample entropy 
%To quantify regularity and complexity in the time series of the TW,
%we estimating the sample entropy from AP and ML axis
M=3;r=0.07;
[e_ML,A_ML,B_ML]=sampenc(ml_norm,M,r);%sample entropy ML
[e_AP,A_AP,B_AP]=sampenc(ap_norm,M,r);%sample entropy AP

%detecting steps and calculating the number of steps, %CV, and step time. 
[Nsteps,step_time,CV, stepIX]=tandem_step_detection(apFilt,timeVec,ml_norm,ap_norm,fs);

%calculating the variability from the ML axis
STD_ML=std(ml_norm);

%mis step occurs between steps 6/7/8

ix1 = stepIX(6):stepIX(7);
ix2 = stepIX(7):stepIX(8);

[dtwML1, ml1_i1, ml2_i1] = dtw(ml(ix1), ml(ix2));
[dtwAP1, ap1_i1, ap2_i1] = dtw(ap(ix1), ap(ix2));

dtwML = eye(Nsteps-1);
dtwAP = eye(Nsteps-1);


for II = 1:Nsteps-2
    for JJ = II+1:Nsteps-1
        dtwML(II,JJ) = dtw(ml(stepIX(II):stepIX(II+1)), ml(stepIX(JJ):stepIX(JJ)+1));
        dtwAP(II,JJ) = dtw(ap(stepIX(II):stepIX(II+1)), ap(stepIX(JJ):stepIX(JJ)+1));
    end
end

    

figure, 
subplot(2,1,1), hold on, plot(apFilt), plot(stepIX, apFilt(stepIX), 'r*');
subplot(2,1,2), hold on, plot(mlFilt), plot(stepIX, mlFilt(stepIX), 'r*');

